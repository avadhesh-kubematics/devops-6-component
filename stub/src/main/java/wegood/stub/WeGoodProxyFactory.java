package wegood.stub;
public class WeGoodProxyFactory implements WeGoodFactory {
    public WeGoodStub createWeGood() {
        return new WeGoodProxy();
    }
}
