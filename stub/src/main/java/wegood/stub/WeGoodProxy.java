package wegood.stub;

import cliffberg.utilities.HttpUtilities;
import java.net.URL;
import java.time.LocalTime;
import java.net.URLEncoder;
import com.google.gson.GsonBuilder;
import com.google.gson.Gson;

public class WeGoodProxy implements WeGoodStub {

    private String serviceURL = null;

    public WeGoodProxy() {
        this.serviceURL = System.getenv("SERVICE_URL");
        if (serviceURL == null) throw new RuntimeException("SERVICE_URL not set");
    }

    public boolean areWeGood() throws Exception {
		String responseStr = HttpUtilities.getHttpResponseAsString(
			new URL(this.serviceURL + "/arewegood"));
		if ((responseStr == null) || responseStr.equals("")) throw new RuntimeException(
			"Internal server error: empty response received from server");
		AreWeGoodResponse response = parseResponse(responseStr);
		return response.areWeGood;
    }

	public void setTime(LocalTime time) throws Exception {
		String encodedTime = URLEncoder.encode(time.toString(), "UTF-8");
		String responseStr = HttpUtilities.getHttpResponseAsString(
			new URL(this.serviceURL + "/settime?time=" + encodedTime));
	}

	private AreWeGoodResponse parseResponse(String jsonStr) throws Exception {

		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		AreWeGoodResponse response;
		try {
			response = gson.fromJson(jsonStr, AreWeGoodResponse.class);
		} catch (Exception ex) {
			throw new RuntimeException("Ill-formatted JSON server response: " + jsonStr);
		}
		return response;
	}

	static class AreWeGoodResponse {
		public AreWeGoodResponse(boolean good) {
			this.areWeGood = good;
		}
		public boolean areWeGood;
	}
}
