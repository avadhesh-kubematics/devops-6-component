package wegood.storytests;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.*;

// Import the Hello stub
import hello.stub.HelloFactory;
import hello.stub.HelloStub;

// Import the WeGood stub
import wegood.stub.WeGoodFactory;
import wegood.stub.WeGoodStub;

import java.time.LocalTime;

public class BasicIT {

	private boolean weGoodResponse = null;
	private WeGoodStub weGood = null;

	public BasicIT() throws Exception {

		// Create stub for accessing the WeGood service:
		String weGoodFactoryClassName = System.getenv("WEGOOD_FACTORY_CLASS_NAME");
		WeGoodFactory weGoodFactory = (WeGoodFactory)
			(Class.forName(weGoodFactoryClassName).getDeclaredConstructor().newInstance());
		this.weGood = weGoodFactory.createWeGood();
	}

	@Given("the time is {string}")
	public void set_time(String timestr) throws Exception {

		// Parse the time string:
		LocalTime localTime = LocalTime.parse(timestr);

		/* Tell the WeGood service to set the time to a specified value. Note that as
			soon as we set it, it starts increasing: */
		this.weGood.setTime(localTime);
	}

	@When("I call WeGood")
	public void call_weGood() throws Exception {
	    this.weGoodResponse = this.weGood.areWeGood();
	}

	@Then("it returns {string}")
	public void it_returns(String expectedResponse) throws Exception {
		assertEquals(expectedResponse, this.weGoodResponse);
	}
}
