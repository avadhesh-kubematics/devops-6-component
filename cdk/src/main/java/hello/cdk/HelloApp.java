package wegood.cdk;

import software.amazon.awscdk.core.App;
import software.amazon.awscdk.core.StackProps;
import software.amazon.awscdk.core.Environment;

public class HelloApp {
    public static void main(final String[] args) {

		String stackName = "WeGoodTestStack";
		String region = "us-east-1";

        App app = new App();
		StackProps props =
			StackProps.builder().env(
				Environment.builder()
				//.account(account)
				.region(region)
				.build())
			.build();

        new HelloStack(app, stackName, props);
        app.synth();
    }
}
